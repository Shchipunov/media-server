# Media server #

Fragments of implementation of a media server.

### Project description ###
The project demonstrates various aspects related to developing a media server:

* Domain model, DAO, Service and Controller layers
* FFmpeg wrapper
* Resource handler that allows to stream audio / video content
* Caching
* Security

The project uses:

* Spring Boot
* Spring (MVC)
* HikariCP
* Ehcache
* Lombok
* Guava
* Thymeleaf