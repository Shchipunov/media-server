package com.shchipunov.utility;

import java.awt.AlphaComposite;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.geom.Rectangle2D;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;

import javax.imageio.ImageIO;

import com.google.common.io.ByteStreams;
import com.shchipunov.component.ResourceWrapper;
import com.shchipunov.exception.UploadResourceException;

/**
 * Presents an utility that allows to do various operations related to image
 * files
 * 
 * @author Shchipunov Stanislav
 */
public class ImageUtility {

	private ImageUtility() {

	}

	public static byte[] getScaledImage(ResourceWrapper imageFile, int width, int height,
			boolean shouldSaveProportion) {
		try {
			try (InputStream inputStream = imageFile.getInputStream()) {
				BufferedImage bufferedImage = ImageIO.read(inputStream);
				if (bufferedImage.getWidth() > width || bufferedImage.getHeight() > height) {
					BufferedImage scaledInstance = getScaledInstance(bufferedImage, width, height,
							shouldSaveProportion);
					return toByteArray(scaledInstance);
				}
			}
			return ByteStreams.toByteArray(imageFile.getInputStream());
		} catch (IOException exception) {
			throw new UploadResourceException("Unable to process image!", exception);
		}
	}

	private static BufferedImage getScaledInstance(BufferedImage bufferedImage, int width, int height,
			boolean shouldSaveProportion) {
		if (shouldSaveProportion) {
			if (bufferedImage.getWidth() > bufferedImage.getHeight()) {
				double ratio = (double) bufferedImage.getHeight() / bufferedImage.getWidth();
				height = (int) Math.round(width * ratio);
			} else {
				double ratio = (double) bufferedImage.getWidth() / bufferedImage.getHeight();
				width = (int) Math.round(height * ratio);
			}
		}
		BufferedImage scaledBufferedImage = new BufferedImage(width, height, bufferedImage.getType());
		Graphics2D canvas = scaledBufferedImage.createGraphics();
		canvas.setRenderingHint(RenderingHints.KEY_INTERPOLATION, RenderingHints.VALUE_INTERPOLATION_BICUBIC);
		canvas.drawImage(bufferedImage, 0, 0, width, height, null);
		canvas.dispose();

		return scaledBufferedImage;
	}

	private static byte[] toByteArray(BufferedImage bufferedImage) throws IOException {
		ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
		ImageIO.write(bufferedImage, "png", byteArrayOutputStream);
		return byteArrayOutputStream.toByteArray();
	}

	public static BufferedImage addSignOnImage(BufferedImage bufferedImage, Position position, Color color,
			int fontSize, String sign) {
		BufferedImage signedBufferedImage = new BufferedImage(bufferedImage.getWidth(), bufferedImage.getHeight(),
				bufferedImage.getType());
		Graphics2D canvas = signedBufferedImage.createGraphics();
		canvas.setRenderingHint(RenderingHints.KEY_INTERPOLATION, RenderingHints.VALUE_INTERPOLATION_BICUBIC);
		canvas.drawImage(bufferedImage, 0, 0, null);
		canvas.setComposite(AlphaComposite.getInstance(AlphaComposite.SRC_OVER, 0.5F));
		canvas.setPaint(color);
		canvas.setFont(new Font("Serif", Font.PLAIN, fontSize));

		Rectangle2D stringBounds = canvas.getFont().getStringBounds(sign, canvas.getFontRenderContext());
		int width = signedBufferedImage.getWidth();
		int height = signedBufferedImage.getHeight();
		double x = 0;
		double y = 0;
		if (position == Position.UPPER_LEFT_CORNER) {
			x = 0;
			y = 0;
		} else if (position == Position.UPPER_RIGHT_CORNER) {
			x = 0;
			y = height - stringBounds.getHeight();
		} else if (position == Position.CENTER) {
			x = (width - stringBounds.getWidth()) / 2;
			y = (height - stringBounds.getHeight()) / 2;
		} else if (position == Position.LOWER_LEFT_CORNER) {
			x = width - stringBounds.getWidth();
			y = 0;
		} else if (position == Position.LOWER_RIGHT_CORNER) {
			x = width - stringBounds.getWidth();
			y = height - stringBounds.getHeight();
		}
		double ascent = -stringBounds.getY();
		double baseline = y + ascent;

		canvas.drawString(sign, (int) x, (int) baseline);
		canvas.dispose();

		return signedBufferedImage;
	}

	public enum Position {
		UPPER_LEFT_CORNER, UPPER_RIGHT_CORNER, CENTER, LOWER_LEFT_CORNER, LOWER_RIGHT_CORNER
	}
}
