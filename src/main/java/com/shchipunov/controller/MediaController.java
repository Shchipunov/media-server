package com.shchipunov.controller;

import java.io.IOException;
import java.util.Objects;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.shchipunov.component.ResourceHandler;
import com.shchipunov.component.ResourceStorage;
import com.shchipunov.component.ResourceWrapper;
import com.shchipunov.exception.ResourceNotFoundException;

/**
 * Handles requests related to audio and video content
 * 
 * @author Shchipunov Stanislav
 */
@Controller
public class MediaController {

	private static final Logger LOGGER = LoggerFactory.getLogger(MediaController.class);

	private final ResourceStorage resourceStorage;

	@Autowired
	public MediaController(ResourceStorage resourceStorage) {
		this.resourceStorage = Objects.requireNonNull(resourceStorage, "Resource storage must not be null!");
	}

	/**
	 * Allows to stream audio and video content
	 * 
	 * @param date
	 * @param resourceName
	 * @param request
	 * @param response
	 */
	@RequestMapping(value = { "/media/{date}/{resourceName:.+}" }, method = RequestMethod.GET)
	public void handleMediaResource(
			@PathVariable String date,
			@PathVariable String resourceName,
			HttpServletRequest request,
			HttpServletResponse response) {
		LOGGER.trace("Date: {}", date);
		LOGGER.trace("Resource name: {}", resourceName);
		String pathToResource = new StringBuilder("media")
				.append("/")
				.append(date)
				.append("/")
				.append(resourceName)
				.toString();
		ResourceWrapper resourceWrapper = resourceStorage.findOne(pathToResource)
				.orElseThrow(() -> new ResourceNotFoundException("Could not find a resource: " + resourceName));
		try {
			ResourceHandler.newInstance(resourceWrapper, request, response).handleRequest();
		} catch (IOException exception) {
			/*
			 * The exception can occur if client-side interrupts the stream to
			 * navigate it
			 */
		}
	}
}
