package com.shchipunov.controller;

import java.util.Objects;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import com.shchipunov.domain.Picture;
import com.shchipunov.exception.UnrecognizedContentException;
import com.shchipunov.service.PictureService;

/**
 * Handles requests related to pictures
 * 
 * @author Shchipunov Stanislav
 */
@RequestMapping("/pictures")
public class PictureController {

	private static final String SCALED_INSTANCE = "scaledInstance";
	private static final String FULL_SIZE_INSTANCE = "fullSizeInstance";

	private final PictureService pictureService;

	@Autowired
	public PictureController(PictureService pictureService) {
		this.pictureService = Objects.requireNonNull(pictureService);
	}

	/**
	 * Retrieves a picture
	 * 
	 * @param id
	 * @param size
	 * @return
	 */
	@GetMapping("/{id}/{size}")
	public ResponseEntity<byte[]> getPhotograph(@PathVariable Long id, @PathVariable String size) {
		Optional<Picture> pictureOptional = Optional.ofNullable(pictureService.findOne(id));
		return pictureOptional
				.map(picture -> {
					byte[] content;
					if (size.equals(SCALED_INSTANCE)) {
						content = picture.getScaledInstance();
					} else if (size.equals(FULL_SIZE_INSTANCE)) {
						content = picture.getFullSizeInstance();
					} else {
						throw new UnrecognizedContentException(String
								.format("Specified invalid size, should be: %s or %s", SCALED_INSTANCE, FULL_SIZE_INSTANCE));
					}
					return ResponseEntity.ok(content);
				})
				.orElseGet(() -> new ResponseEntity<>(HttpStatus.NOT_FOUND));
	}
}
