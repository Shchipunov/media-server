package com.shchipunov.controller;

import java.net.URI;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.UUID;
import java.util.stream.Collectors;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.util.UriComponentsBuilder;

import com.google.common.collect.ImmutableSet;
import com.shchipunov.component.JobProgress;
import com.shchipunov.component.MultipartFileWrapperImplementation;
import com.shchipunov.domain.AudioInformation;
import com.shchipunov.domain.FormatType;
import com.shchipunov.domain.MediaInformation;
import com.shchipunov.domain.Picture;
import com.shchipunov.domain.VideoInformation;
import com.shchipunov.exception.UploadResourceException;
import com.shchipunov.service.MediaService;
import com.shchipunov.utility.ImageUtility;

/**
 * Handles requests related to uploading of files
 * 
 * @author Shchipunov Stanislav
 */
@Controller
@PreAuthorize("hasRole('ROLE_ADMINISTRATOR')")
@RequestMapping("/administrator")
public class AdministratorController {

	private static final Logger LOGGER = LoggerFactory.getLogger(AdministratorController.class);
	
	private static final int SCALED_IMAGE_WIDTH = 460;
	private static final int SCALED_IMAGE_HEIGHT = 250;
	
	/**
	 * Set of allowed types of images
	 */
	private static final Set<String> ALLOWED_IMAGE_TYPES = new ImmutableSet.Builder<String>()
			.add("image/gif")
			.add("image/jpeg")
			.add("image/png")
			.build();
	/**
	 * Set of allowed types of audio
	 */
	private static final Set<String> ALLOWED_AUDIO_TYPES = new ImmutableSet.Builder<String>()
			.add("audio/aac")
			.add("audio/mpeg")
			.add("audio/ogg")
			.add("audio/vorbis")
			.build();
	/**
	 * Set of allowed types of video
	 */
	private static final Set<String> ALLOWED_VIDEO_TYPES = new ImmutableSet.Builder<String>()
			.add("video/mp4")
			.add("video/ogg")
			.add("video/quicktime")
			.add("video/webm")
			.build();
	
	private final MediaService mediaService;

	@Autowired
	public AdministratorController(MediaService mediaService) {
		this.mediaService = Objects.requireNonNull(mediaService, "Media service must not be null!");
	}

	@GetMapping
	public String showAdministratorPage() {
		return "administratorPage";
	}

	/**
	 * Allows to upload audio files
	 * 
	 * @param audioInformation
	 * @param bindingResult
	 * @param audioFile
	 * @param imageFiles
	 * @param processUUID
	 * @param builder
	 * @return
	 */
	@PostMapping("/audio")
	public ResponseEntity<?> uploadAudio(@Valid AudioInformation audioInformation, BindingResult bindingResult,
			@RequestPart MultipartFile audioFile,
			@RequestPart(name = "imageFiles", required = false) List<MultipartFile> imageFiles, UUID processUUID, UriComponentsBuilder builder) {
		if (bindingResult.hasErrors()) {
			return ResponseEntity.badRequest().build();
		}
		if (!matchesAllowedAudioTypes(audioFile.getContentType())) {
			return new ResponseEntity<String>("Not allowed audio type!", HttpStatus.BAD_REQUEST);
		}
		if (!imageFiles.isEmpty()) {
			LOGGER.trace("Image files array size: {}", imageFiles.size());
			List<Picture> pictures = imageFiles.stream()
					.filter(imageFile -> matchesAllowedImageTypes(imageFile.getContentType()))
					.map(imageFile -> ImageUtility.getScaledImage(new MultipartFileWrapperImplementation(imageFile),
							SCALED_IMAGE_WIDTH, SCALED_IMAGE_HEIGHT, true))
					.map(scaledInstance -> new Picture(scaledInstance, null))
					.collect(Collectors.toList());
			audioInformation.setPictures(pictures);
		}
		MediaInformation mediaInformation = mediaService.save(processUUID, audioInformation,
				new MultipartFileWrapperImplementation(audioFile));
		URI uri = builder
				.path(mediaInformation.getPathToResource(FormatType.MP3))
				.build()
				.toUri();
		return ResponseEntity.created(uri).build();
	}
	
	/**
	 * Allows to upload video files
	 * 
	 * @param videoInformation
	 * @param bindingResult
	 * @param videoFile
	 * @param processUUID
	 * @param builder
	 * @return
	 */
	@PostMapping("/video")
	@ResponseStatus(HttpStatus.CREATED)
	public ResponseEntity<?> uploadVideo(@Valid VideoInformation videoInformation, BindingResult bindingResult,
			@RequestPart MultipartFile videoFile, @ModelAttribute("processUUID") UUID processUUID,
			UriComponentsBuilder builder) {
		if (bindingResult.hasErrors()) {
			return ResponseEntity.badRequest().build();
		}
		if (!matchesAllowedVideoTypes(videoFile.getContentType())) {
			return new ResponseEntity<String>("Not allowed video type!", HttpStatus.BAD_REQUEST);
		}
		MediaInformation mediaInformation = mediaService.save(processUUID, videoInformation,
				new MultipartFileWrapperImplementation(videoFile));
		URI uri = builder
				.path(mediaInformation.getPathToResource(FormatType.MP4_720p))
				.build()
				.toUri();
		return ResponseEntity.created(uri).build();
	}

	/**
	 * Allows to get job progress
	 * 
	 * @param processUUID
	 * @return
	 */
	@GetMapping("/progress")
	public @ResponseBody JobProgress getJobProgress(UUID processUUID) {
		return Optional.ofNullable(mediaService.getJobProgress(processUUID))
				.orElseThrow(() -> new IllegalArgumentException("No active process found!"));
	}

	/**
	 * Handles {@link UploadResourceException} exception
	 * 
	 * @param exception
	 * @return
	 */
	@ExceptionHandler(UploadResourceException.class)
	public ResponseEntity<String> handleUploadResourceException(UploadResourceException exception) {
		Optional.ofNullable(exception.getCause()).ifPresent(cause -> cause.printStackTrace());
		return new ResponseEntity<String>(exception.getMessage(), HttpStatus.BAD_REQUEST);
	}

	/* Internal methods */

	private static boolean matchesAllowedImageTypes(String contentType) {
		return ALLOWED_IMAGE_TYPES.contains(contentType);
	}

	private static boolean matchesAllowedAudioTypes(String contentType) {
		return ALLOWED_AUDIO_TYPES.contains(contentType);
	}

	private static boolean matchesAllowedVideoTypes(String contentType) {
		return ALLOWED_VIDEO_TYPES.contains(contentType);
	}

}
