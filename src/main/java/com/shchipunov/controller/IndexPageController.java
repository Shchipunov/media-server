package com.shchipunov.controller;

import java.util.List;
import java.util.Objects;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;

import com.shchipunov.domain.MediaInformation;
import com.shchipunov.service.MediaService;

/**
 * @author Shchipunov Stanislav
 */
@Controller
public class IndexPageController {

	private final MediaService mediaService;

	@Autowired
	public IndexPageController(MediaService mediaService) {
		this.mediaService = Objects.requireNonNull(mediaService, "Media service must not be null!");

	}

	@ModelAttribute("resources")
	public List<MediaInformation> populateResources() {
		return mediaService.findAll();
	}

	/**
	 * Shows index page
	 * 
	 * @return
	 */
	@GetMapping("/")
	public String showIndexPage() {
		return "indexPage";
	}
}
