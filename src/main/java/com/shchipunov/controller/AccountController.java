package com.shchipunov.controller;

import java.security.Principal;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * Handles user's requests
 * 
 * @author Shchipunov Stanislav
 */
@Controller
public class AccountController {

	@RequestMapping(value = "/logIn", method = RequestMethod.GET)
	public String showLogInPage(Principal principal) {
		return principal != null ? "redirect:/accounts/" + principal.getName() : "logInPage";
	}
}
