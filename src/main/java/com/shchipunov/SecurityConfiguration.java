package com.shchipunov;

import java.util.Map;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.method.configuration.GlobalMethodSecurityConfiguration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.web.authentication.AuthenticationFailureHandler;
import org.springframework.security.web.authentication.ExceptionMappingAuthenticationFailureHandler;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;
import org.thymeleaf.extras.springsecurity4.dialect.SpringSecurityDialect;

import com.google.common.collect.ImmutableMap;
import com.shchipunov.repository.AccountDAO;

/**
 * Security configuration
 * 
 * @author Shchipunov Stanislav
 */
@Configuration
@EnableWebSecurity
public class SecurityConfiguration extends WebSecurityConfigurerAdapter {
	
	public static final String BAD_CREDENTIALS = "badCredentials";
	public static final String DISABLED = "disabled";

	@Autowired
	private AccountDAO accountDAO;

	/**
	 * Customized user details service
	 */
	@Override
	protected void configure(AuthenticationManagerBuilder authenticationManagerBuilder) throws Exception {
		authenticationManagerBuilder.userDetailsService(new UserDetailsService() {
			@Override
			public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
				return Optional.ofNullable(accountDAO.findOne(username))
						.orElseThrow(() -> new UsernameNotFoundException("Could not find: " + username));
			}
		});
	}

	@Override
	public void configure(WebSecurity webSecurity) {
		webSecurity.ignoring().antMatchers("/resources/static/**");
	}

	/**
	 * Main security configuration
	 */
	@Override
	protected void configure(HttpSecurity httpSecurity) throws Exception {
		httpSecurity
			.authorizeRequests()
				.antMatchers("/administrator/**").hasRole("ADMINISTRATOR")
				.antMatchers("/dashboard").authenticated()
				.anyRequest().permitAll()
			.and().formLogin()
				.loginPage("/logIn")
				.usernameParameter("username")
				.passwordParameter("password")
				.loginProcessingUrl("/authenticate")
				.failureHandler(authenticationFailureHandler())
				.permitAll()
			.and().logout()
				.logoutRequestMatcher(new AntPathRequestMatcher("/logOut"))
				.invalidateHttpSession(true)
				.deleteCookies("JSESSIONID")
				.logoutSuccessUrl("/logIn")
			.and().csrf().disable();
	}

	@Bean
	public SpringSecurityDialect springSecurityDialect() {
		return new SpringSecurityDialect();
	}

	/**
	 * Customized authentication failure handler
	 * 
	 * @return
	 */
	@Bean
	public AuthenticationFailureHandler authenticationFailureHandler() {
		ExceptionMappingAuthenticationFailureHandler authenticationFailureHandler = new ExceptionMappingAuthenticationFailureHandler();
		Map<String, String> map = new ImmutableMap.Builder<String, String>()
				.put("org.springframework.security.authentication.BadCredentialsException", "/logIn?error=" + BAD_CREDENTIALS)
				.put("org.springframework.security.authentication.DisabledException", "/logIn?error=" + DISABLED)
				.build();
		authenticationFailureHandler.setExceptionMappings(map);
		return authenticationFailureHandler;
	}

	/**
	 * Annotation-based method security
	 */
	@Configuration
	@EnableGlobalMethodSecurity(prePostEnabled = true)
	public static class MethodSecurityConfiguration extends GlobalMethodSecurityConfiguration {

	}
}
