package com.shchipunov.exception;

/**
 * @author Shchipunov Stanislav
 */
public class UnrecognizedContentException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	public UnrecognizedContentException(String message) {
		super(message);
	}
}
