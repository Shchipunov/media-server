package com.shchipunov.exception;

/**
 * @author Shchipunov Stanislav
 */
public class UploadResourceException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	public UploadResourceException(String message) {
		super(message);
	}

	public UploadResourceException(String message, Throwable throwable) {
		super(message, throwable);
	}
}
