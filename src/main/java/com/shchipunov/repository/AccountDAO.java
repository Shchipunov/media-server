package com.shchipunov.repository;

import com.shchipunov.domain.Account;

/**
 * @author Shchipunov Stanislav
 */
public interface AccountDAO extends JpaRepository<Account, String> {

}
