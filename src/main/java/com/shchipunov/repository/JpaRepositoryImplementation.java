package com.shchipunov.repository;

import java.io.Serializable;
import java.lang.reflect.ParameterizedType;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaQuery;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

/**
 * Presents an implementation of {@link JpaRepository<T, K>}
 * 
 * @author Shchipunov Stanislav
 */
@Repository
@Transactional
public abstract class JpaRepositoryImplementation<T, K extends Serializable> implements JpaRepository<T, K> {

	@PersistenceContext
	protected EntityManager entityManager;

	protected Class<T> entityClass;

	@SuppressWarnings("unchecked")
	public JpaRepositoryImplementation() {
		ParameterizedType genericSuperclass = (ParameterizedType) getClass().getGenericSuperclass();
		this.entityClass = (Class<T>) genericSuperclass.getActualTypeArguments()[0];
	}

	@Override
	@Transactional(readOnly = true)
	public T findOne(K id) {
		return entityManager.find(entityClass, id);
	}

	@Override
	@Transactional(readOnly = true)
	public List<T> findAll() {
		CriteriaQuery<T> criteriaQuery = entityManager.getCriteriaBuilder().createQuery(entityClass);
		criteriaQuery.select(criteriaQuery.from(entityClass));
		return entityManager.createQuery(criteriaQuery).getResultList();
	}

	@Override
	public T save(T object) {
		entityManager.persist(object);
		return object;
	}

	@Override
	public T update(T object) {
		return entityManager.merge(object);
	}

	@Override
	public void delete(T object) {
		entityManager.remove(entityManager.merge(object));
	}

}
