package com.shchipunov.repository;

import java.util.List;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.shchipunov.component.PageHolder;
import com.shchipunov.domain.MediaInformation;

/**
 * Repository to manage {@link MediaInformation} instances.
 * 
 * @author Shchipunov Stanislav
 */
@Repository
public class MediaInformationDAOImplementation extends JpaRepositoryImplementation<MediaInformation, Long>
		implements MediaInformationDAO {

	@Override
	@Transactional(readOnly = true)
	public List<MediaInformation> findByTitle(String title) {
		return entityManager
				.createNamedQuery("MediaInformation.findByTitle", MediaInformation.class)
				.setParameter("title", "%" + title + "%")
				.getResultList();
	}

	@Override
	@Transactional(readOnly = true)
	public PageHolder<MediaInformation> findByKeywords(List<String> keywords, Integer pageNumber, Integer pageSize) {
		pageNumber = pageNumber != null ? pageNumber : PageHolder.DEFAULT_PAGE_NUMBER;
		pageSize = pageSize != null ? pageSize : PageHolder.DEFAULT_PAGE_SIZE;
		
		Long totalElements = entityManager
				.createNamedQuery("MediaInformation.getCountByKeywords", Long.class)
				.setParameter("keywords", keywords)
				.getSingleResult();
		int totalPages = (int) (totalElements / pageSize);
		if (!(((double) totalElements / pageSize) % 1 == 0)) {
			totalPages += 1;
		}
		List<MediaInformation> foundElements = entityManager
				.createNamedQuery("MediaInformation.findByKeywords", MediaInformation.class)
				.setParameter("keywords", keywords)
				.setFirstResult(pageSize * (pageNumber - 1))
				.setMaxResults(pageSize)
				.getResultList();
		return new PageHolder<MediaInformation>(pageNumber, pageSize, totalPages, totalElements, foundElements);
	}

}
