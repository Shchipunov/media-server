package com.shchipunov.repository;

import org.springframework.stereotype.Repository;

import com.shchipunov.domain.Account;

/**
 * Repository to manage {@link Account} instances.
 * 
 * @author Shchipunov Stanislav
 */
@Repository
public class AccountDAOImplementation extends JpaRepositoryImplementation<Account, String> implements AccountDAO {

}
