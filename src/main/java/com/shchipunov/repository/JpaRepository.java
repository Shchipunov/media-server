package com.shchipunov.repository;

import java.io.Serializable;
import java.util.List;

/**
 * Generic interface (CRUD)
 * 
 * @author Shchipunov Stanislav
 */
public interface JpaRepository<T, K extends Serializable> {

	T findOne(K id);
	List<T> findAll();
	T save(T object);
	T update(T object);
	void delete(T object);
}
