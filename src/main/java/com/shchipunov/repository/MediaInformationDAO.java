package com.shchipunov.repository;

import java.util.List;

import com.shchipunov.component.PageHolder;
import com.shchipunov.domain.MediaInformation;

/**
 * @author Shchipunov Stanislav
 */
public interface MediaInformationDAO extends JpaRepository<MediaInformation, Long> {

	List<MediaInformation> findByTitle(String title);

	PageHolder<MediaInformation> findByKeywords(List<String> keywords, Integer pageNumber, Integer pageSize);
}
