package com.shchipunov.repository;

import org.springframework.stereotype.Repository;

import com.shchipunov.domain.Picture;

/**
 * Repository to manage {@link Picture} instances.
 * 
 * @author Shchipunov Stanislav
 */
@Repository
public class PictureDAOImplementation extends JpaRepositoryImplementation<Picture, Long> implements PictureDAO {

}
