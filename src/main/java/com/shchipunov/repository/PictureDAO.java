package com.shchipunov.repository;

import com.shchipunov.domain.Picture;

/**
 * @author Shchipunov Stanislav
 */
public interface PictureDAO extends JpaRepository<Picture, Long> {

}
