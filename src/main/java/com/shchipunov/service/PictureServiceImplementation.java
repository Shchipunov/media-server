package com.shchipunov.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.shchipunov.domain.Picture;
import com.shchipunov.repository.PictureDAO;

/**
 * Presents a class that handles {@link Picture} instances. It delegates
 * persistence operations to {@link PictureDAO}
 * 
 * @author Shchipunov Stanislav
 */
@Service
public class PictureServiceImplementation implements PictureService {

	private final PictureDAO pictureDAO;

	@Autowired
	public PictureServiceImplementation(PictureDAO pictureDAO) {
		this.pictureDAO = pictureDAO;
	}

	@Override
	public Picture findOne(Long id) {
		return pictureDAO.findOne(id);
	}

}
