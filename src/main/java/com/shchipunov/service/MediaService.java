package com.shchipunov.service;

import java.util.List;
import java.util.UUID;

import com.shchipunov.component.JobProgress;
import com.shchipunov.component.ResourceWrapper;
import com.shchipunov.domain.AudioInformation;
import com.shchipunov.domain.MediaInformation;
import com.shchipunov.domain.VideoInformation;
import com.shchipunov.exception.UploadResourceException;

/**
 * @author Shchipunov Stanislav
 */
public interface MediaService {

	List<MediaInformation> findAll();

	MediaInformation save(UUID processUUID, AudioInformation audioInformation, ResourceWrapper audioFile)
			throws UploadResourceException;

	MediaInformation save(UUID processUUID, VideoInformation videoInformation, ResourceWrapper videoFile)
			throws UploadResourceException;

	JobProgress getJobProgress(UUID processUUID);
}
