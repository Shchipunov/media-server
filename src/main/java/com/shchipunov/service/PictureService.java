package com.shchipunov.service;

import com.shchipunov.domain.Picture;

/**
 * @author Shchipunov Stanislav
 */
public interface PictureService {

	Picture findOne(Long id);

}
