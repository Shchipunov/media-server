package com.shchipunov.service;

import static java.util.Objects.requireNonNull;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.UUID;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import com.shchipunov.component.Encoder;
import com.shchipunov.component.JobProgress;
import com.shchipunov.component.ResourceStorage;
import com.shchipunov.component.ResourceWrapper;
import com.shchipunov.domain.AudioInformation;
import com.shchipunov.domain.FormatType;
import com.shchipunov.domain.MediaInformation;
import com.shchipunov.domain.VideoInformation;
import com.shchipunov.exception.UploadResourceException;
import com.shchipunov.repository.MediaInformationDAO;

/**
 * Presents a class that handles {@link MediaInformation} instances and
 * {@link ResourceWrapper}
 * 
 * @author Shchipunov Stanislav
 */
@Service
public class MediaServiceImplementation implements MediaService {

	private static final Logger LOGGER = LoggerFactory.getLogger(MediaServiceImplementation.class);

	/**
	 * Map that holds job progresses
	 */
	private final Map<UUID, JobProgress> jobProgresses = new HashMap<>();

	private final ResourceStorage resourceStorage;
	private final Encoder audioEncoder;
	private final Encoder videoEncoder;
	private final MediaInformationDAO mediaInformationDAO;

	/**
	 * Creates a new {@link MediaServiceImplementation}
	 * 
	 * @param resourceStorage
	 *            must not be {@literal null}.
	 * @param audioEncoder
	 *            must not be {@literal null}.
	 * @param videoEncoder
	 *            must not be {@literal null}.
	 * @param mediaInformationDAO
	 *            must not be {@literal null}.
	 */
	@Autowired
	public MediaServiceImplementation(ResourceStorage resourceStorage, @Qualifier("audioEncoder") Encoder audioEncoder,
			@Qualifier("videoEncoder") Encoder videoEncoder, MediaInformationDAO mediaInformationDAO) {
		this.resourceStorage = requireNonNull(resourceStorage);
		this.audioEncoder = requireNonNull(audioEncoder);
		this.videoEncoder = requireNonNull(videoEncoder);
		this.mediaInformationDAO = requireNonNull(mediaInformationDAO);
	}

	@Override
	public List<MediaInformation> findAll() {
		return mediaInformationDAO.findAll();
	}

	@Override
	public MediaInformation save(UUID processUUID, AudioInformation audioInformation, ResourceWrapper audioFile)
			throws UploadResourceException {
		return saveInternal(processUUID, audioEncoder, audioInformation, audioFile);
	}

	@Override
	public MediaInformation save(UUID processUUID, VideoInformation videoInformation, ResourceWrapper videoFile)
			throws UploadResourceException {
		return saveInternal(processUUID, videoEncoder, videoInformation, videoFile);
	}

	@Override
	public JobProgress getJobProgress(UUID processUUID) {
		return jobProgresses.get(processUUID);
	}

	private MediaInformation saveInternal(UUID processUUID, Encoder encoder, MediaInformation mediaInformation,
			ResourceWrapper inputResource) throws UploadResourceException {
		jobProgresses.put(processUUID, new JobProgress());
		Map<FormatType, String> temporaryPathsToResource = null;
		Map<FormatType, String> permanentPathsToResource = null;
		try {
			temporaryPathsToResource = encoder.encode(inputResource, percentageCompletion -> jobProgresses
					.get(processUUID).setPercentageCompletion(percentageCompletion));
			LOGGER.trace("Temporary paths to resource: {}", temporaryPathsToResource);
			permanentPathsToResource = resourceStorage.transfer(temporaryPathsToResource);
			LOGGER.trace("Permanent paths to resource: {}", permanentPathsToResource);
			mediaInformation.setPathsToResource(permanentPathsToResource);
			return mediaInformationDAO.save(mediaInformation);
		} catch (Exception exception) {
			if (temporaryPathsToResource != null) {
				temporaryPathsToResource.values().forEach(this::deleteIfExists);
			}
			if (permanentPathsToResource != null) {
				temporaryPathsToResource.values().forEach(resourceStorage::delete);
			}
			throw new UploadResourceException(
					String.format("Unable to upload a file: %s", inputResource.getOriginalName()), exception);
		}
	}

	private void deleteIfExists(String pathToResource) {
		try {
			Files.deleteIfExists(Paths.get(pathToResource));
		} catch (IOException exception) {
			LOGGER.warn("Could not delete a file: {}, cause: {}", pathToResource, exception.getMessage());
		}
	}

	@Scheduled(cron = "0 0 5 * * ?")
	private void removeOldJobProgresses() {
		jobProgresses.entrySet().removeIf(this::checkConditions);
	}

	private boolean checkConditions(Entry<UUID, JobProgress> entry) {
		LocalDateTime now = LocalDateTime.now();
		JobProgress jobProgress = entry.getValue();
		return jobProgress.getLastUpdated().plusHours(1).isBefore(now);
	}

}
