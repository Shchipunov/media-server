package com.shchipunov;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

/**
 * @author Shchipunov Stanislav
 */
@SpringBootApplication
@EnableScheduling
public class Launcher {

	public static void main(String[] arguments) {
		SpringApplication.run(Launcher.class, arguments);
	}
}
