package com.shchipunov.component;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.util.Objects;

import com.shchipunov.utility.DigestUtility;

/**
 * Presents an implementation of {@link ResourceWrapper}
 * 
 * @author Shchipunov Stanislav
 */
public class FileWrapperImplementation implements ResourceWrapper {

	private final File file;

	/**
	 * Creates a new {@link FileWrapperImplementation} that wraps the given
	 * {@link File}
	 * 
	 * @param multipartFile
	 *            must not be {@literal null}.
	 */
	public FileWrapperImplementation(File file) {
		this.file = Objects.requireNonNull(file, "Resource must not be null!");
	}

	@Override
	public String getOriginalName() {
		return file.getName();
	}

	@Override
	public String getContentType() {
		try {
			return Files.probeContentType(file.toPath());
		} catch (IOException exception) {
			return null;
		}
	}

	@Override
	public String getETag() {
		return "\"" + DigestUtility.hash(file, "SHA-512") + "\"";
	}

	@Override
	public InputStream getInputStream() {
		try {
			return new BufferedInputStream(new FileInputStream(file));
		} catch (IOException exception) {
			throw new IllegalArgumentException(exception);
		}
	}

	@Override
	public long getContentLength() {
		return file.length();
	}

	@Override
	public long getLastModified() {
		return file.lastModified();
	}
}
