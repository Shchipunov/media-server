package com.shchipunov.component;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.Map;

import org.springframework.stereotype.Service;

import com.google.common.collect.ImmutableMap;
import com.shchipunov.component.ffmpeg.FFmpegTask;
import com.shchipunov.component.ffmpeg.ProgressListener;
import com.shchipunov.domain.FormatType;

/**
 * Presents a service that encapsulate business logic to encode audio files
 * 
 * @author Shchipunov Stanislav
 */
@Service("audioEncoder")
public class AudioEncoderImplementation extends AbstractEncoder {

	private static final String MP3_EXTENSION = ".mp3";

	@Override
	public Map<FormatType, String> encode(ResourceWrapper inputResource, ProgressListener progressListener)
			throws IOException {
		Path pathToInputFile = Files.createFile(createPathToInputFile(
				inputResource.getOriginalName().substring(inputResource.getOriginalName().lastIndexOf("."))));
		Files.copy(inputResource.getInputStream(), pathToInputFile, StandardCopyOption.REPLACE_EXISTING);

		Map<FormatType, String> pathsToResource = ImmutableMap.<FormatType, String> builder()
				.put(FormatType.MP3, createPathToOutputFile(MP3_EXTENSION))
				.build();

		FFmpegTask task = FFmpegTask.builder()
				.setInputFile(pathToInputFile.toString())
				.deleteInputFileOnExit(true)
				.overrideOutputFiles(true)
				.addOutputFile(pathsToResource.get(FormatType.MP3))
					.withAudioOptions()
						.setCodec("libmp3lame")
						.setSamplingFrequency(44100)
						.setBitRate(128000)
						.setNumberOfChannels(2)
					.and()
				.and()
				.build();
		try {
			executor.execute(task, progressListener);
		} catch (IOException exception) {
			Files.deleteIfExists(pathToInputFile);
			for (String pathToResource : pathsToResource.values()) {
				Files.deleteIfExists(Paths.get(pathToResource));
			}
			throw new IOException(exception);
		}
		return pathsToResource;
	}

}
