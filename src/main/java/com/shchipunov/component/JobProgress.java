package com.shchipunov.component;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * Presents a class that holds information about progress of some process
 * 
 * @author Shchipunov Stanislav
 */
public class JobProgress implements Serializable {

	private static final long serialVersionUID = 1L;

	private int percentageCompletion;
	private LocalDateTime lastUpdated;

	public JobProgress() {
		this.lastUpdated = LocalDateTime.now();
	}

	public int getPercentageCompletion() {
		return percentageCompletion;
	}

	public void setPercentageCompletion(int percentageCompletion) {
		this.percentageCompletion = percentageCompletion;
		this.lastUpdated = LocalDateTime.now();
	}

	public LocalDateTime getLastUpdated() {
		return lastUpdated;
	}

	public boolean isInProgress() {
		return percentageCompletion != 0 && percentageCompletion != 100;
	}

	public boolean isCompleted() {
		return percentageCompletion == 100;
	}

}
