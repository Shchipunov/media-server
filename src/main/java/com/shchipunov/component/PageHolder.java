package com.shchipunov.component;

import java.util.List;

/**
 * Presents a class that is used for pagination. It holds content of a page and
 * common information.
 * 
 * @author Shchipunov Stanislav
 */
public class PageHolder<T> {

	public static final int DEFAULT_PAGE_NUMBER = 1;
	public static final int DEFAULT_PAGE_SIZE = 1;

	private static final int FIRST_PAGE_NUMBER = 1;
	private static final int PAGES_BEFORE_CURRENT_PAGE = 2;
	private static final int PAGES_AFTER_FIRST_PAGE = 4;

	private final int startIndex;
	private final int endIndex;
	private final boolean first;
	private final boolean last;
	private final int currentPage;
	private final int currentSize;
	private final int totalPages;
	private final long totalElements;
	private final List<T> content;

	public PageHolder(int currentPage, int currentSize, int totalPages, long totalElements, List<T> content) {
		int startIndex = Math.max(FIRST_PAGE_NUMBER, currentPage - PAGES_BEFORE_CURRENT_PAGE);
		int endIndex = Math.min(startIndex + PAGES_AFTER_FIRST_PAGE, totalPages);
		int differenceBetweenIndexes = endIndex - startIndex;
		if (differenceBetweenIndexes < PAGES_AFTER_FIRST_PAGE && startIndex != FIRST_PAGE_NUMBER) {
			int desiredStartIndex = startIndex - (PAGES_AFTER_FIRST_PAGE - differenceBetweenIndexes);
			while (startIndex > FIRST_PAGE_NUMBER && startIndex != desiredStartIndex) {
				startIndex--;
			}
		}
		this.startIndex = startIndex;
		this.endIndex = endIndex;
		this.first = currentPage == FIRST_PAGE_NUMBER;
		this.last = currentPage == totalPages;
		this.currentPage = currentPage;
		this.currentSize = currentSize;
		this.totalPages = totalPages;
		this.totalElements = totalElements;
		this.content = content;
	}

	public int getStartIndex() {
		return startIndex;
	}

	public int getEndIndex() {
		return endIndex;
	}

	public boolean isFirst() {
		return first;
	}

	public boolean isLast() {
		return last;
	}

	public int getCurrentPage() {
		return currentPage;
	}

	public int getCurrentSize() {
		return currentSize;
	}

	public int getTotalPages() {
		return totalPages;
	}

	public long getTotalElements() {
		return totalElements;
	}

	public List<T> getContent() {
		return content;
	}
}
