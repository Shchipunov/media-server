package com.shchipunov.component;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

/**
 * Presents an interface with default methods that allow to generate a path to
 * permanent location
 * 
 * @author Shchipunov Stanislav
 */
public interface PathGenerator {

	default String next() {
		String date = DateTimeFormatter.BASIC_ISO_DATE.format(LocalDate.now());
		return new StringBuilder()
				.append("media")
				.append("/")
				.append(date)
				.toString();
	}
	
	default String next(String resourceName) {
		return new StringBuilder(next())
				.append("/")
				.append(resourceName)
				.toString();
	}
}
