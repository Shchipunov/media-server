package com.shchipunov.component;

import static org.springframework.http.HttpHeaders.ACCEPT_RANGES;
import static org.springframework.http.HttpHeaders.CONTENT_RANGE;
import static org.springframework.http.HttpHeaders.ETAG;
import static org.springframework.http.HttpHeaders.EXPIRES;
import static org.springframework.http.HttpHeaders.LAST_MODIFIED;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.charset.StandardCharsets;
import java.util.List;
import java.util.Random;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpRange;
import org.springframework.http.server.ServletServerHttpRequest;
import org.springframework.web.context.request.ServletWebRequest;

/**
 * @author Shchipunov Stanislav
 */
public class ResourceHandler {

	private static final Logger LOGGER = LoggerFactory.getLogger(ResourceHandler.class);

	private static final byte[] BOUNDARY_CHARS = new byte[] { '-', '_', '1', '2', '3', '4', '5', '6', '7', '8', '9',
			'0', 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't',
			'u', 'v', 'w', 'x', 'y', 'z', 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O',
			'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z' };
	private static final Random RANDOM = new Random();

	private static final int DEFAULT_BUFFER_SIZE = 20480; // 20 KB
	private static final long DEFAULT_EXPIRY_TIME = 604800000L; // 1 week

	private final ResourceWrapper resourceWrapper;
	private final HttpServletRequest request;
	private final HttpServletResponse response;

	private ResourceHandler(ResourceWrapper resourceWrapper, HttpServletRequest request, HttpServletResponse response) {
		this.resourceWrapper = resourceWrapper;
		this.request = request;
		this.response = response;
	}

	public static ResourceHandler newInstance(ResourceWrapper resourceWrapper, HttpServletRequest request,
			HttpServletResponse response) {
		return new ResourceHandler(resourceWrapper, request, response);
	}

	public void handleRequest() throws IOException {
		if (new ServletWebRequest(request, response).checkNotModified(resourceWrapper.getETag(),
				resourceWrapper.getLastModified())) {
			LOGGER.trace("Resource not modified: {}", resourceWrapper.getOriginalName());
			return;
		}
		long contentLength = resourceWrapper.getContentLength();
		List<HttpRange> ranges;
		try {
			HttpHeaders headers = new ServletServerHttpRequest(request).getHeaders();
			ranges = headers.getRange();
		} catch (IllegalArgumentException exception) {
			response.addHeader(CONTENT_RANGE, "bytes */" + contentLength);
			response.sendError(HttpServletResponse.SC_REQUESTED_RANGE_NOT_SATISFIABLE);
			return;
		}

		response.setStatus(HttpServletResponse.SC_PARTIAL_CONTENT);

		if (ranges.size() == 1) {
			HttpRange range = ranges.get(0);

			long start = range.getRangeStart(contentLength);
			long end = range.getRangeEnd(contentLength);
			long rangeLength = end - start + 1;

			setHeaders(response, resourceWrapper);
			response.addHeader(CONTENT_RANGE, "bytes " + start + "-" + end + "/" + contentLength);
			response.setContentLength((int) rangeLength);

			try (InputStream inputStream = new BufferedInputStream(resourceWrapper.getInputStream());
					OutputStream outputStream = response.getOutputStream()) {
				copyRange(inputStream, outputStream, start, end);
			}
		} else {
			String boundary = generateMultipartBoundary();
			response.setContentType("multipart/byteranges; boundary=" + boundary);

			try (InputStream inputStream = new BufferedInputStream(resourceWrapper.getInputStream());
					ServletOutputStream outputStream = response.getOutputStream()) {
				for (HttpRange range : ranges) {
					long start = range.getRangeStart(contentLength);
					long end = range.getRangeEnd(contentLength);

					outputStream.println();
					outputStream.println("--" + boundary);
					String contentType = resourceWrapper.getContentType();
					if (contentType != null) {
						outputStream.println("Content-Type: " + contentType);
					}
					outputStream.println("Content-Range: bytes " + start + "-" + end + "/" + contentLength);
					outputStream.println();

					copyRange(inputStream, outputStream, start, end);
				}
				outputStream.println();
				outputStream.print("--" + boundary + "--");
			}
		}
	}

	private void setHeaders(HttpServletResponse response, ResourceWrapper resourceWrapper) throws IOException {
		response.setContentType(resourceWrapper.getContentType());
		response.setHeader(ETAG, resourceWrapper.getETag());
		response.setDateHeader(LAST_MODIFIED, resourceWrapper.getLastModified());
		response.setDateHeader(EXPIRES, System.currentTimeMillis() + DEFAULT_EXPIRY_TIME);
		response.setHeader(ACCEPT_RANGES, "bytes");
	}

	private void copyRange(InputStream inputStream, OutputStream outputStream, long start, long end)
			throws IOException {
		inputStream.skip(start);

		long bytesToCopy = end - start + 1;
		byte buffer[] = new byte[DEFAULT_BUFFER_SIZE];
		while (bytesToCopy > 0) {
			int bytesRead = inputStream.read(buffer);
			if (bytesRead <= bytesToCopy) {
				outputStream.write(buffer, 0, bytesRead);
				bytesToCopy -= bytesRead;
			} else {
				outputStream.write(buffer, 0, (int) bytesToCopy);
				bytesToCopy = 0;
			}
			if (bytesRead < buffer.length) {
				break;
			}
		}
	}

	private static String generateMultipartBoundary() {
		byte[] multipartBoundary = new byte[RANDOM.nextInt(11) + 30];
		for (int i = 0; i < multipartBoundary.length; i++) {
			multipartBoundary[i] = BOUNDARY_CHARS[RANDOM.nextInt(BOUNDARY_CHARS.length)];
		}
		return new String(multipartBoundary, StandardCharsets.US_ASCII);
	}
}
