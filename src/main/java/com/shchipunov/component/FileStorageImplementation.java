package com.shchipunov.component;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Objects;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.shchipunov.domain.FormatType;

/**
 * Presents an implementation of {@link ResourceStorage}
 * 
 * @author Shchipunov Stanislav
 */
@Component
public class FileStorageImplementation implements ResourceStorage {

	private static final Logger LOGGER = LoggerFactory.getLogger(FileStorageImplementation.class);

	public static final String ROOT_FOLDER = new File(File.separator).getAbsolutePath();

	private final PathGenerator pathGenerator;

	/**
	 * Creates a new {@link FileStorageImplementation}
	 * 
	 * @param pathGenerator
	 *            must not be {@literal null}.
	 */
	@Autowired
	public FileStorageImplementation(PathGenerator pathGenerator) {
		this.pathGenerator = Objects.requireNonNull(pathGenerator);
	}

	/**
	 * Finds a resource
	 */
	@Override
	public Optional<ResourceWrapper> findOne(String pathToResource) {
		LOGGER.debug("Path to resource: {}", pathToResource);

		ResourceWrapper resourceWrapper = new FileWrapperImplementation(new File(ROOT_FOLDER, pathToResource));
		return Optional.of(resourceWrapper);
	}

	/**
	 * Saves a resource
	 */
	@Override
	public void save(String pathToResource, ResourceWrapper resourceWrapper) {
		LOGGER.debug("Path to resource: {}", pathToResource);
		LOGGER.debug("Name: {}", resourceWrapper.getOriginalName());
		LOGGER.debug("Content type: {}", resourceWrapper.getContentType());
		LOGGER.debug("Content length: {}", resourceWrapper.getContentLength());

		Path outputPath = Paths.get(ROOT_FOLDER, pathToResource);
		try {
			if (!Files.exists(outputPath.getParent())) {
				Files.createDirectories(outputPath.getParent());
			}
			Files.copy(resourceWrapper.getInputStream(), outputPath, StandardCopyOption.REPLACE_EXISTING);
		} catch (IOException exception) {
			throw new RuntimeException(exception);
		}
	}

	/**
	 * Deletes a resource
	 */
	@Override
	public void delete(String pathToResource) {
		try {
			Files.deleteIfExists(Paths.get(pathToResource));
		} catch (IOException exception) {
			LOGGER.warn("Could not delete a file: {}, cause: {}", pathToResource, exception.getMessage());
		}
	}

	/**
	 * Transfers files from temporary to permanent location
	 */
	@Override
	public Map<FormatType, String> transfer(Map<FormatType, String> temporaryPathsToResource) {
		Map<FormatType, String> permanentPathsToResource = new HashMap<>(temporaryPathsToResource.size());
		for (Entry<FormatType, String> entry : temporaryPathsToResource.entrySet()) {
			String temporaryPathToResource = entry.getValue();
			String resourceName = temporaryPathToResource
					.substring(temporaryPathToResource.lastIndexOf(File.separator));
			Path permanentPathToResource = Paths.get(ROOT_FOLDER, pathGenerator.next(resourceName));
			try {
				if (!Files.exists(permanentPathToResource.getParent())) {
					Files.createDirectories(permanentPathToResource.getParent());
				}
				Files.move(Paths.get(temporaryPathToResource), permanentPathToResource, StandardCopyOption.ATOMIC_MOVE);
			} catch (IOException exception) {
				permanentPathsToResource.values().forEach(this::delete);
				throw new RuntimeException(String.format("Could not move a file: %s", temporaryPathToResource),
						exception);
			}
			permanentPathsToResource.put(entry.getKey(), encodePathToResource(permanentPathToResource.toString()));
		}
		return permanentPathsToResource;
	}

	private String encodePathToResource(String pathToResource) {
		return pathToResource
				.replace(ROOT_FOLDER, "/")
				.replace(File.separator, "/");
	}

}
