package com.shchipunov.component;

import java.io.IOException;
import java.util.Map;

import com.shchipunov.component.ffmpeg.ProgressListener;
import com.shchipunov.domain.FormatType;

/**
 * @author Shchipunov Stanislav
 */
public interface Encoder {

	Map<FormatType, String> encode(ResourceWrapper inputResource, ProgressListener progressListener) throws IOException;
}
