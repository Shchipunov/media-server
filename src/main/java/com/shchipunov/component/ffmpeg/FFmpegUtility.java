package com.shchipunov.component.ffmpeg;

import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author Shchipunov Stanislav
 */
public class FFmpegUtility {

	private static final Pattern PROGRESS_INFORMATION_PATTERN = Pattern.compile("\\s*(\\w+)\\s*=\\s*(\\S+)\\s*",
			Pattern.CASE_INSENSITIVE);
	private static final Pattern TIME_SECTION_PATTERN = Pattern.compile("(\\d\\d):(\\d\\d):(\\d\\d)\\.(\\d\\d).*$",
			Pattern.CASE_INSENSITIVE);

	private FFmpegUtility() {

	}

	public static String convertMillisecondsToTime(long milliseconds) {
		long seconds = milliseconds / 1000;
		milliseconds = milliseconds % 1000;
		long minutes = seconds / 60;
		seconds = seconds % 60;
		long hours = minutes / 60;
		minutes = minutes % 60;

		return String.format("%02d:%02d:%02d.%03d", hours, minutes, seconds, milliseconds);
	}

	public static long convertTimeToMilliseconds(String time) {
		Matcher matcher = TIME_SECTION_PATTERN.matcher(time);
		if (!matcher.matches()) {
			throw new IllegalArgumentException(String.format("Cannot parse time: %s", time));
		}
		long hours = Long.parseLong(matcher.group(1));
		long minutes = Long.parseLong(matcher.group(2));
		long seconds = Long.parseLong(matcher.group(3));
		long truncatedMilliseconds = Long.parseLong(matcher.group(4));

		return hours * 60 * 60 * 1000 + minutes * 60 * 1000 + seconds * 1000 + truncatedMilliseconds * 10;
	}

	public static Map<String, String> parseProgressInformation(String progressInformaton) {
		Map<String, String> parsedProgressInformation = new HashMap<>();
		Matcher matcher = PROGRESS_INFORMATION_PATTERN.matcher(progressInformaton);
		while (matcher.find()) {
			String sectionName = matcher.group(1);
			String sectionValue = matcher.group(2);
			parsedProgressInformation.put(sectionName, sectionValue);
		}
		return parsedProgressInformation;
	}
}
