package com.shchipunov.component.ffmpeg;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Strings.isNullOrEmpty;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import com.google.common.collect.ImmutableList;

/**
 * Presents a task to execute
 * 
 * @author Shchipunov Stanislav
 */
public class FFmpegTask {

	private final String pathToInputFile;
	private final boolean deleteInputFileOnExit;
	private final boolean overrideOutputFiles;
	private final List<OutputConfiguration> outputConfigurations;

	private FFmpegTask(String pathToInputFile, boolean deleteInputFileOnExit, boolean overrideOutputFiles,
			List<OutputConfiguration> outputConfigurations) {
		this.pathToInputFile = pathToInputFile;
		this.deleteInputFileOnExit = deleteInputFileOnExit;
		this.overrideOutputFiles = overrideOutputFiles;
		this.outputConfigurations = outputConfigurations;
	}

	/**
	 * @return Path to input file
	 */
	protected String getPathToInputFile() {
		return pathToInputFile;
	}

	/**
	 * @return output configurations
	 */
	protected List<OutputConfiguration> getOutputConfigurations() {
		return Collections.unmodifiableList(outputConfigurations);
	}

	/**
	 * Build arguments to execute
	 * 
	 * @return list of arguments
	 */
	public List<String> buildArguments() {
		ImmutableList.Builder<String> arguments = new ImmutableList.Builder<>();
		arguments.add(overrideOutputFiles ? "-y" : "-n");
		arguments.add("-i", pathToInputFile);
		outputConfigurations.stream().map(OutputConfiguration::build).forEach(arguments::addAll);

		return arguments.build();
	}

	/**
	 * @return {@link Builder} for customization of a task
	 */
	public static Builder builder() {
		return new Builder();
	}

	public boolean isDeleteInputFileOnExit() {
		return deleteInputFileOnExit;
	}

	/**
	 * {@link Builder} that build a task to execute
	 * 
	 * @author Shchipunov Stanislav
	 */
	public static final class Builder {

		private String pathToInputFile;
		private boolean deleteInputFileOnExit;
		private boolean overrideOutputFiles;
		private final List<OutputConfiguration> outputConfigurations = new ArrayList<>();

		public Builder setInputFile(String pathToFile) {
			checkArgument(!isNullOrEmpty(pathToFile), "Path to input file must not be null or empty!");
			this.pathToInputFile = pathToFile;
			return this;
		}

		public Builder deleteInputFileOnExit(boolean deleteInputFileOnExit) {
			this.deleteInputFileOnExit = deleteInputFileOnExit;
			return this;
		}

		public Builder overrideOutputFiles(boolean overrideOutputFiles) {
			this.overrideOutputFiles = overrideOutputFiles;
			return this;
		}

		public OutputConfiguration addOutputFile(String pathToFile) {
			OutputConfiguration outputConfiguration = new OutputConfiguration(pathToFile, this);
			outputConfigurations.add(outputConfiguration);
			return outputConfiguration;
		}

		public FFmpegTask build() {
			checkArgument(!isNullOrEmpty(pathToInputFile), "Input file must be specified!");
			checkArgument(!outputConfigurations.isEmpty(), "At least one output file must be specified!");

			return new FFmpegTask(pathToInputFile, deleteInputFileOnExit, overrideOutputFiles, outputConfigurations);
		}
	}

}
