package com.shchipunov.component.ffmpeg;

/**
 * @author Shchipunov Stanislav
 */
public interface ExecutableFilesPointer {

	String getPathToFFmpeg();

	String getPathToFFprobe();
}
