package com.shchipunov.component.ffmpeg;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;

import com.google.common.base.MoreObjects;

/**
 * Presents an implementation of {@link ExecutableFilesPointer}
 * 
 * @author Shchipunov Stanislav
 */
public class ExecutableFilesPointerImplementation implements ExecutableFilesPointer {

	private static final String OPERATING_SYSTEM = System.getProperty("os.name").toLowerCase();
	private static final String FILE_EXTENSION = OPERATING_SYSTEM.contains("windows") ? ".exe" : "";
	private static final Path TEMPORARY_DIRECTORY = Paths.get(System.getProperty("java.io.tmpdir"),
			"com.shchipunov.ffmpeg");

	private String pathToFFmpeg;
	private String pathToFFprobe;

	public ExecutableFilesPointerImplementation() {

	}

	public ExecutableFilesPointerImplementation(String pathToFFmpeg, String pathToFFprobe) {
		this.pathToFFmpeg = pathToFFmpeg;
		this.pathToFFprobe = pathToFFprobe;
	}

	@Override
	public String getPathToFFmpeg() {
		return MoreObjects.firstNonNull(pathToFFmpeg, copyToTemporaryFile("ffmpeg" + FILE_EXTENSION));
	}

	@Override
	public String getPathToFFprobe() {
		return MoreObjects.firstNonNull(pathToFFprobe, copyToTemporaryFile("ffprobe" + FILE_EXTENSION));
	}

	private String copyToTemporaryFile(String fileName) {
		try {
			if (Files.notExists(TEMPORARY_DIRECTORY)) {
				Files.createDirectory(TEMPORARY_DIRECTORY);
			}
			Path pathToExecutableFile = Paths.get(TEMPORARY_DIRECTORY.toString(), fileName);
			if (Files.notExists(pathToExecutableFile)) {
				Files.copy(getClass().getResourceAsStream("/" + fileName), pathToExecutableFile,
						StandardCopyOption.REPLACE_EXISTING);
			}
			return pathToExecutableFile.toString();
		} catch (IOException exception) {
			throw new RuntimeException(String.format("Could not create a temporary file: %s", fileName), exception);
		}
	}
}
