package com.shchipunov.component.ffmpeg;

/**
 * @author Shchipunov Stanislav
 */
public interface ProgressListener {

	void progressChanged(int percentageCompletion);
}
