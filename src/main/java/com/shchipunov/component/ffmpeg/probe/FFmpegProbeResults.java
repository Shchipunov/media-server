package com.shchipunov.component.ffmpeg.probe;

import java.util.Collections;
import java.util.List;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Presents a class that holds all results of probe
 * 
 * @author Shchipunov Stanislav
 */
public class FFmpegProbeResults extends FFmpegAbstractProbeResults {

	private FFmpegProbeFormat format;
	private List<FFmpegProbeStream> streams;

	public FFmpegProbeFormat getFormat() {
		return format;
	}

	public List<FFmpegProbeStream> getStreams() {
		return Collections.unmodifiableList(streams);
	}

	public static final class FFmpegProbeFormat extends FFmpegAbstractProbeResults {

		private double duration;
		private long size;
		private Map<String, String> tags;

		public double getDuration() {
			return duration;
		}

		public long getSize() {
			return size;
		}

		public Map<String, String> getTags() {
			return Collections.unmodifiableMap(tags);
		}

	}

	public static final class FFmpegProbeStream extends FFmpegAbstractProbeResults {

		@JsonProperty("codec_name")
		private String codecName;
		@JsonProperty("codec_type")
		private String codecType;
		private int width;
		private int height;
		private double duration;
		private Map<String, String> tags;
		private Map<String, String> disposition;

		public String getCodecName() {
			return codecName;
		}

		public String getCodecType() {
			return codecType;
		}

		public int getWidth() {
			return width;
		}

		public int getHeight() {
			return height;
		}

		public double getDuration() {
			return duration;
		}

		public Map<String, String> getTags() {
			return Collections.unmodifiableMap(tags);
		}

		public Map<String, String> getDisposition() {
			return Collections.unmodifiableMap(disposition);
		}

	}
}
