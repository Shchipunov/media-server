package com.shchipunov.component.ffmpeg.probe;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonAutoDetect;

/**
 * Presents an abstract class that holds a map of properties
 * 
 * @author Shchipunov Stanislav
 */
@JsonAutoDetect(fieldVisibility = JsonAutoDetect.Visibility.ANY)
public abstract class FFmpegAbstractProbeResults {

	private Map<String, Object> properties = new HashMap<>();

	@JsonAnySetter
	private void setProperty(String name, Object value) {
		properties.put(name, value);
	}

	public Map<String, Object> getProperties() {
		return Collections.unmodifiableMap(properties);
	}

	public Object getProperty(String name) {
		return properties.get(name);
	}

}
