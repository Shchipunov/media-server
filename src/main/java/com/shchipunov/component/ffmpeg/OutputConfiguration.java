package com.shchipunov.component.ffmpeg;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Strings.isNullOrEmpty;
import static com.shchipunov.component.ffmpeg.FFmpegUtility.convertMillisecondsToTime;
import static java.util.Objects.requireNonNull;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import com.google.common.collect.ImmutableList;
import com.shchipunov.component.ffmpeg.FFmpegTask.Builder;

/**
 * Presents a class that holds output configuration
 * 
 * @author Shchipunov Stanislav
 */
public class OutputConfiguration {
	
	public static final long UNDEFINED_OFFSET = -1;
	public static final long UNDEFINED_DURATION = -1;

	private final Builder builder;

	private String pathToOutputFile;

	private long offset = UNDEFINED_OFFSET;
	private long duration = UNDEFINED_DURATION;
	
	private VideoOptions videoOptions;
	private AudioOptions audioOptions;

	private final List<String> extraArguments = new ArrayList<>();

	public OutputConfiguration(String pathToOutputFile, Builder builder) {
		checkArgument(!isNullOrEmpty(pathToOutputFile), "Path to input file must not be null or empty!");
		this.pathToOutputFile = pathToOutputFile;
		this.builder = requireNonNull(builder);
	}
	
	public long getOffset() {
		return offset;
	}
	
	public OutputConfiguration setOffset(long offset, TimeUnit timeUnit) {
		checkArgument(offset > 0, "Offset must be greater than zero!");
		requireNonNull(timeUnit);
		this.offset = TimeUnit.MILLISECONDS.convert(offset, timeUnit);
		return this;
	}
	
	public long getDuration() {
		return duration;
	}

	public OutputConfiguration setDuration(long duration, TimeUnit timeUnit) {
		checkArgument(duration > 0, "Duration must be greater than zero!");
		requireNonNull(timeUnit);
		this.duration = TimeUnit.MILLISECONDS.convert(duration, timeUnit);
		return this;
	}

	public OutputConfiguration addExtraArguments(String... arguments) {
		checkArgument(arguments.length > 0, "At least one argument must be specified!");
		for (String argument : arguments) {
			extraArguments.add(requireNonNull(argument));
		}
		return this;
	}

	public VideoOptions withVideoOptions() {
		this.videoOptions = new VideoOptions();
		return videoOptions;
	}

	/**
	 * Presents a class that holds video options
	 * 
	 * @author Shchipunov Stanislav
	 */
	public final class VideoOptions {

		private int numberOfFrames;
		private int width;
		private int height;
		private boolean enabled = true;
		private String codec;
		private String filter;
		private String pixelFormat;
		private String movFlags;
		private int bitRate;

		/**
		 * Set the number of video frames to output
		 * 
		 * @param numberOfFrames
		 * @return this
		 */
		public VideoOptions setNumberOfFrames(int numberOfFrames) {
			checkArgument(numberOfFrames > 0, "Number of video frames must be greater than zero!");
			this.numberOfFrames = numberOfFrames;
			return this;
		}

		/**
		 * Set frame size
		 * 
		 * @param width
		 * @param height
		 * @return this
		 */
		public VideoOptions setFrameSize(int width, int height) {
			checkArgument(width > 0 && height > 0, "Both video width and video height must be greater than zero!");
			this.width = width;
			this.height = height;
			return this;
		}

		/**
		 * Disable video recording
		 * 
		 * @return this
		 */
		public VideoOptions disableVideo() {
			this.enabled = false;
			return this;
		}

		/**
		 * Set the video codec
		 * 
		 * @param codec
		 * @return this
		 */
		public VideoOptions setCodec(String codec) {
			checkArgument(!isNullOrEmpty(codec), "Video codec must not be null or empty!");
			this.codec = codec;
			return this;
		}

		public VideoOptions setFilter(String filter) {
			checkArgument(!isNullOrEmpty(filter), "Video filter must not be null or empty!");
			this.filter = filter;
			return this;
		}

		/**
		 * Set pixel format
		 * 
		 * @param pixelFormat
		 * @return this
		 */
		public VideoOptions setPixelFormat(String pixelFormat) {
			checkArgument(!isNullOrEmpty(pixelFormat), "Pixel format must not be null or empty!");
			this.pixelFormat = pixelFormat;
			return this;
		}

		public VideoOptions setMovFlags(String movFlags) {
			checkArgument(!isNullOrEmpty(movFlags), "MOV flags must not be null or empty!");
			this.movFlags = movFlags;
			return this;
		}

		public VideoOptions setBitRate(int bitRate) {
			checkArgument(bitRate > 0, "Video bit rate must be greater than zero!");
			this.bitRate = bitRate;
			return this;
		}

		public OutputConfiguration and() {
			return OutputConfiguration.this;
		}
	}

	public AudioOptions withAudioOptions() {
		this.audioOptions = new AudioOptions();
		return audioOptions;
	}

	/**
	 * Presents a class that holds video options
	 * 
	 * @author Shchipunov Stanislav
	 */
	public final class AudioOptions {

		private int numberOfFrames;
		private int samplingFrequency;
		private int numberOfChannels;
		private boolean enabled = true;
		private String codec;
		private int bitRate;

		/**
		 * Set the number of audio frames to output
		 * 
		 * @param numberOfFrames
		 * @return this
		 */
		public AudioOptions setNumberOfFrames(int numberOfFrames) {
			checkArgument(numberOfFrames > 0, "Number of audio frames must be greater than zero!");
			this.numberOfFrames = numberOfFrames;
			return this;
		}

		/**
		 * Set the audio sampling frequency
		 * 
		 * @param samplingFrequency
		 * @return this
		 */
		public AudioOptions setSamplingFrequency(int samplingFrequency) {
			checkArgument(samplingFrequency > 0, "Audio sampling frequency must be greater than zero!");
			this.samplingFrequency = samplingFrequency;
			return this;
		}

		/**
		 * Set the number of audio channels
		 * 
		 * @param numberOfChannels
		 * @return this
		 */
		public AudioOptions setNumberOfChannels(int numberOfChannels) {
			checkArgument(numberOfChannels > 0, "Number of audio channels must be greater than zero!");
			this.numberOfChannels = numberOfChannels;
			return this;
		}

		/**
		 * Disable audio recording
		 * 
		 * @return this
		 */
		public AudioOptions disableAudio() {
			this.enabled = false;
			return this;
		}

		/**
		 * Set the audio codec
		 * 
		 * @param codec
		 * @return this
		 */
		public AudioOptions setCodec(String codec) {
			checkArgument(!isNullOrEmpty(codec), "Audio codec must not be null or empty!");
			this.codec = codec;
			return this;
		}

		public AudioOptions setBitRate(int bitRate) {
			checkArgument(bitRate > 0, "Audio bit rate must be greater than zero!");
			this.bitRate = bitRate;
			return this;
		}

		public OutputConfiguration and() {
			return OutputConfiguration.this;
		}
	}

	public Builder and() {
		return builder;
	}

	protected List<String> build() {
		ImmutableList.Builder<String> arguments = new ImmutableList.Builder<>();

		if (offset > 0) {
			arguments.add("-ss", convertMillisecondsToTime(offset));
		}
		if (duration > 0) {
			arguments.add("-t", convertMillisecondsToTime(duration));
		}
		if (videoOptions != null) {
			if (videoOptions.enabled) {
				if (videoOptions.numberOfFrames > 0) {
					arguments.add("-vframes", Integer.toString(videoOptions.numberOfFrames));
				}
				if (videoOptions.width != 0 && videoOptions.height != 0) {
					arguments.add("-s", String.format("%dx%d", videoOptions.width, videoOptions.height));
				}
				if (!isNullOrEmpty(videoOptions.codec)) {
					arguments.add("-vcodec", videoOptions.codec);
				}
				if (!isNullOrEmpty(videoOptions.filter)) {
					arguments.add("-vf", videoOptions.filter);
				}
				if (!isNullOrEmpty(videoOptions.pixelFormat)) {
					arguments.add("-pix_fmt", videoOptions.pixelFormat);
				}
				if (!isNullOrEmpty(videoOptions.movFlags)) {
					arguments.add("-movflags", videoOptions.movFlags);
				}
				if (videoOptions.bitRate > 0) {
					arguments.add("-b:v", Integer.toString(videoOptions.bitRate));
				}
			} else {
				arguments.add("-vn");
			}
		}
		if (audioOptions != null) {
			if (audioOptions.enabled) {
				if (audioOptions.numberOfFrames > 0) {
					arguments.add("-aframes", Integer.toString(audioOptions.numberOfFrames));
				}
				if (audioOptions.samplingFrequency > 0) {
					arguments.add("-ar", Integer.toString(audioOptions.samplingFrequency));
				}
				if (audioOptions.numberOfChannels > 0) {
					arguments.add("-ac", Integer.toString(audioOptions.numberOfChannels));
				}
				if (!isNullOrEmpty(audioOptions.codec)) {
					arguments.add("-acodec", audioOptions.codec);
				}
				if (audioOptions.bitRate > 0) {
					arguments.add("-a:v", Integer.toString(audioOptions.bitRate));
				}
			} else {
				arguments.add("-an");
			}
		}
		arguments.addAll(extraArguments);
		arguments.add(pathToOutputFile);

		return arguments.build();
	}

}
