package com.shchipunov.component.ffmpeg;

import static com.shchipunov.component.ffmpeg.FFmpegUtility.convertTimeToMilliseconds;
import static com.shchipunov.component.ffmpeg.FFmpegUtility.parseProgressInformation;
import static com.shchipunov.component.ffmpeg.OutputConfiguration.UNDEFINED_DURATION;
import static com.shchipunov.component.ffmpeg.OutputConfiguration.UNDEFINED_OFFSET;
import static java.util.Objects.requireNonNull;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;
import java.util.LongSummaryStatistics;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.collect.ImmutableList;
import com.google.common.io.CharStreams;
import com.shchipunov.component.ffmpeg.probe.FFmpegProbeResults;

/**
 * Presents a class that executes tasks
 * 
 * @author Shchipunov Stanislav
 */
public class FFmpegExecutor {

	private static final Logger LOGGER = LoggerFactory.getLogger(FFmpegExecutor.class);

	private static final String TIME_SECTION = "time";

	private final ExecutableFilesPointer executableFilesPointer;

	public FFmpegExecutor() {
		this(new ExecutableFilesPointerImplementation());
	}

	public FFmpegExecutor(ExecutableFilesPointer executableFilesPointer) {
		this.executableFilesPointer = executableFilesPointer;
	}

	public FFmpegExecutor(String pathToFFmpeg, String pathToFFprobe) {
		this(new ExecutableFilesPointerImplementation(
				requireNonNull(pathToFFmpeg, "Path to FFmpeg executable file must not be null!"),
				requireNonNull(pathToFFprobe, "Path to FFprobe executable file must not be null!")));
	}

	/**
	 * @param task
	 *            {@link FFmpegTask} instance to execute
	 * @throws IOException
	 */
	public void execute(FFmpegTask task) throws IOException {
		execute(task, null);
	}

	/**
	 * @param task
	 *            {@link FFmpegTask} instance to execute
	 * @param listener
	 *            {@link ProgressListener} instance to listen progress status
	 * @throws IOException
	 */
	public void execute(FFmpegTask task, ProgressListener listener) throws IOException {
		requireNonNull(task, "Task must not be null!");

		LOGGER.debug("Parameters of command: {}", task.buildArguments());

		List<String> arguments = new ImmutableList.Builder<String>()
				.add(executableFilesPointer.getPathToFFmpeg())
				.addAll(task.buildArguments())
				.build();
		Process process = new ProcessHolder().run(arguments);
		try {
			try (BufferedReader reader = new BufferedReader(
					new InputStreamReader(process.getInputStream(), StandardCharsets.UTF_8))) {
				if (listener != null) {
					LongSummaryStatistics offsetSummaryStatistics = task.getOutputConfigurations().stream()
							.mapToLong(OutputConfiguration::getOffset)
							.summaryStatistics();
					LongSummaryStatistics durationSummaryStatistics = task.getOutputConfigurations().stream()
							.mapToLong(OutputConfiguration::getDuration)
							.summaryStatistics();
					long durationInMilliseconds = durationSummaryStatistics.getMin() == UNDEFINED_DURATION
							? UNDEFINED_DURATION : durationSummaryStatistics.getMax();
					long offsetInMilliseconds = offsetSummaryStatistics.getMin() == UNDEFINED_OFFSET ? 0
							: offsetSummaryStatistics.getMin();
					if (durationInMilliseconds == UNDEFINED_DURATION) {
						FFmpegProbeResults probeResult = probe(task.getPathToInputFile());
						durationInMilliseconds = Math.round(probeResult.getFormat().getDuration() * 1000);
					}
					durationInMilliseconds -= offsetInMilliseconds;

					String line;
					while ((line = reader.readLine()) != null) {
						Map<String, String> parsedProgressInformation = parseProgressInformation(line);
						String time = parsedProgressInformation.get(TIME_SECTION);
						if (time != null) {
							long processedTimeInMilliseconds = convertTimeToMilliseconds(time);
							int percentageCompletion = (int) Math
									.round((double) (processedTimeInMilliseconds * 100) / durationInMilliseconds);
							listener.progressChanged(percentageCompletion);
						}
					}
					if (task.isDeleteInputFileOnExit()) {
						Files.deleteIfExists(Paths.get(task.getPathToInputFile()));
					}
				} else {
					CharStreams.copy(reader, System.out);
				}
			}

		} finally {
			process.destroy();
		}
	}

	public FFmpegProbeResults probe(String pathToFile) {
		ImmutableList.Builder<String> arguments = new ImmutableList.Builder<>();
		arguments.add(executableFilesPointer.getPathToFFprobe())
				.add("-v", "quiet")
				.add("-print_format", "json")
				.add("-show_format")
				.add("-show_streams")
				.add(pathToFile);
		try {
			Process process = new ProcessHolder().run(arguments.build());
			try {
				try (BufferedReader reader = new BufferedReader(new InputStreamReader(process.getInputStream()))) {
					ObjectMapper objectMapper = new ObjectMapper();
					FFmpegProbeResults probeResults = objectMapper.readValue(reader, FFmpegProbeResults.class);
					return probeResults;
				}
			} finally {
				process.destroy();
			}
		} catch (IOException exception) {
			throw new RuntimeException(exception);
		}
	}
}
