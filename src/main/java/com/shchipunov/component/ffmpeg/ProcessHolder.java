package com.shchipunov.component.ffmpeg;

import java.io.IOException;
import java.util.List;
import java.util.Objects;

import com.google.common.base.Preconditions;

/**
 * @author Shchipunov Stanislav
 */
public class ProcessHolder {

	public Process run(List<String> arguments) throws IOException {
		Objects.requireNonNull(arguments, "Arguments must not be null!");
		Preconditions.checkArgument(!arguments.isEmpty(), "No arguments specified");

		return new ProcessBuilder(arguments)
				.redirectErrorStream(true)
				.start();
	}
}
