package com.shchipunov.component;

import java.util.Map;
import java.util.Optional;

import com.shchipunov.domain.FormatType;

/**
 * @author Shchipunov Stanislav
 */
public interface ResourceStorage {

	Optional<ResourceWrapper> findOne(String pathToResource);

	Map<FormatType, String> transfer(Map<FormatType, String> temporaryPathsToResource);

	void save(String pathToResource, ResourceWrapper resourceWrapper);

	void delete(String pathToResource);
}