package com.shchipunov.component;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Map;
import java.util.UUID;

import com.shchipunov.component.ffmpeg.FFmpegExecutor;
import com.shchipunov.component.ffmpeg.ProgressListener;
import com.shchipunov.domain.FormatType;

/**
 * Presents an abstract class that holds some basic properties and methods
 * 
 * @author Shchipunov Stanislav
 */
public abstract class AbstractEncoder implements Encoder {

	/**
	 * Path to temporary directory where all temporary files will be stored
	 */
	private static final Path TEMPORARY_DIRECTORY = Paths.get(System.getProperty("java.io.tmpdir"),
			"com.shchipunov.ffmpeg.temporary");

	protected final FFmpegExecutor executor;

	public AbstractEncoder() {
		if (!Files.exists(TEMPORARY_DIRECTORY)) {
			try {
				Files.createDirectory(TEMPORARY_DIRECTORY);
			} catch (IOException exception) {
				throw new RuntimeException(
						String.format("Could not create temporary directory: %s", TEMPORARY_DIRECTORY));
			}
		}
		this.executor = new FFmpegExecutor();
	}

	@Override
	public abstract Map<FormatType, String> encode(ResourceWrapper inputResource, ProgressListener progressListener)
			throws IOException;

	protected Path createPathToInputFile(String fileExtension) {
		return TEMPORARY_DIRECTORY.resolve(UUID.randomUUID().toString() + fileExtension);
	}

	protected String createPathToOutputFile(String fileExtension) {
		return TEMPORARY_DIRECTORY.resolve(UUID.randomUUID().toString() + fileExtension).toString();
	}
}
