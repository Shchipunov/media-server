package com.shchipunov.component;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.util.Date;
import java.util.Objects;

import com.shchipunov.utility.DigestUtility;

/**
 * Presents an implementation of {@link ResourceWrapper}
 * 
 * @author Shchipunov Stanislav
 */
public class ByteArrayWrapperImplementation implements ResourceWrapper {

	private final byte[] content;
	private final String originalName;
	private final String contentType;
	private final long lastModified;

	/**
	 * Creates a new {@link ByteArrayWrapperImplementation} that wraps the given
	 * byte array
	 * 
	 * @param content
	 *            must not be {@literal null}.
	 * @param originalName
	 *            must not be {@literal null}.
	 * @param contentType
	 *            must not be {@literal null}.
	 */
	public ByteArrayWrapperImplementation(byte[] content, String originalName, String contentType) {
		this.content = Objects.requireNonNull(content);
		this.originalName = Objects.requireNonNull(originalName);
		this.contentType = Objects.requireNonNull(contentType);
		this.lastModified = new Date().getTime();
	}

	@Override
	public String getOriginalName() {
		return originalName;
	}

	@Override
	public InputStream getInputStream() {
		return new ByteArrayInputStream(content);
	}

	@Override
	public long getContentLength() {
		return content.length;
	}

	@Override
	public String getContentType() {
		return contentType;
	}

	@Override
	public String getETag() {
		return "\"" + DigestUtility.hash(content, "SHA-512") + "\"";
	}

	@Override
	public long getLastModified() {
		return lastModified;
	}
}
