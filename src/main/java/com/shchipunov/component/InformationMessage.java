package com.shchipunov.component;

/**
 * @author Shchipunov Stanislav
 */
public class InformationMessage {

	private final InformationMessageType type;
	private final String text;

	public InformationMessage(InformationMessageType type, String text) {
		this.type = type;
		this.text = text;
	}

	public InformationMessageType getType() {
		return type;
	}

	public String getText() {
		return text;
	}

	public enum InformationMessageType {
		FAILURE, SUCCESS
	}
}
