package com.shchipunov.component;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.Map;

import org.springframework.stereotype.Service;

import com.google.common.collect.ImmutableMap;
import com.shchipunov.component.ffmpeg.FFmpegTask;
import com.shchipunov.component.ffmpeg.ProgressListener;
import com.shchipunov.domain.FormatType;

/**
 * Presents a service that encapsulate business logic to encode video files
 * 
 * @author Shchipunov Stanislav
 */
@Service("videoEncoder")
public class VideoEncoderImplementation extends AbstractEncoder {

	private static final String MP4_EXTENSION = ".mp4";

	@Override
	public Map<FormatType, String> encode(ResourceWrapper inputResource, ProgressListener progressListener)
			throws IOException {
		Path pathToInputFile = Files.createFile(createPathToInputFile(
				inputResource.getOriginalName().substring(inputResource.getOriginalName().lastIndexOf("."))));
		Files.copy(inputResource.getInputStream(), pathToInputFile, StandardCopyOption.REPLACE_EXISTING);

		Map<FormatType, String> pathsToResource = ImmutableMap.<FormatType, String> builder()
				.put(FormatType.MP4_1080p, createPathToOutputFile(MP4_EXTENSION))
				.put(FormatType.MP4_720p, createPathToOutputFile(MP4_EXTENSION))
				.put(FormatType.MP4_360p, createPathToOutputFile(MP4_EXTENSION))
				.build();

		FFmpegTask task = FFmpegTask.builder()
				.setInputFile(pathToInputFile.toString())
				.deleteInputFileOnExit(true)
				.overrideOutputFiles(true)
				.addOutputFile(pathsToResource.get(FormatType.MP4_1080p))
					.withVideoOptions()
						.setCodec("libx264")
						.setPixelFormat("yuv420p")
						.setFilter("scale=-1:1080")
						.setMovFlags("+faststart")
					.and()
				.and()
				.addOutputFile(pathsToResource.get(FormatType.MP4_720p))
					.withVideoOptions()
						.setCodec("libx264")
						.setPixelFormat("yuv420p")
						.setFilter("scale=-1:720")
						.setMovFlags("+faststart")
					.and()
				.and()
				.addOutputFile(pathsToResource.get(FormatType.MP4_360p))
					.withVideoOptions()
						.setCodec("libx264")
						.setPixelFormat("yuv420p")
						.setFilter("scale=-1:360")
						.setMovFlags("+faststart")
					.and()
				.and()
				.build();
		try {
			executor.execute(task, progressListener);
		} catch (IOException exception) {
			Files.deleteIfExists(pathToInputFile);
			for (String pathToResource : pathsToResource.values()) {
				Files.deleteIfExists(Paths.get(pathToResource));
			}
			throw new IOException(exception);
		}
		return pathsToResource;
	}

}
