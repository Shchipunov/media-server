package com.shchipunov;

import java.lang.reflect.Method;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.springframework.cache.annotation.EnableCaching;
import org.springframework.cache.interceptor.KeyGenerator;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * Cache configuration
 * 
 * @author Shchipunov Stanislav
 */
@Configuration
@EnableCaching
public class CacheConfiguration {

	public static final String ACCOUNT_CACHE = "accountCache";
	public static final String MESSAGE_CACHE = "messageCache";
	public static final String PICTURE_CACHE = "pictureCache";

	@Bean
	public KeyGenerator keyGenerator() {
		return new KeyGenerator() {
			@Override
			public Object generate(Object object, Method method, Object... arguments) {
				String firstPartOfKey = Stream.of(arguments)
						.map(Object::hashCode)
						.map(String::valueOf)
						.collect(Collectors.joining());
				return firstPartOfKey + method.getName().hashCode();
			}
		};
	}
}
