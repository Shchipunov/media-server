package com.shchipunov.domain;

/**
 * @author Shchipunov Stanislav
 */
public enum FormatType {

	MP4_360p, MP4_720p, MP4_1080p, MP3
}
