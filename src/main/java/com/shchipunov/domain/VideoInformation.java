package com.shchipunov.domain;

import javax.persistence.Entity;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * Presents a class that holds additional information about the resource
 * 
 * @author Shchipunov Stanislav
 */
@Entity
@Getter
@Setter
@ToString(callSuper = true)
public class VideoInformation extends MediaInformation {

	private Integer year;
}
