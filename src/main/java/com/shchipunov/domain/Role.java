package com.shchipunov.domain;

/**
 * @author Shchipunov Stanislav
 */
public enum Role {

	ROLE_USER, ROLE_ADMINISTRATOR
}
