package com.shchipunov.domain;

import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import javax.persistence.CollectionTable;
import javax.persistence.Column;
import javax.persistence.DiscriminatorColumn;
import javax.persistence.DiscriminatorType;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.MapKeyColumn;
import javax.persistence.MapKeyEnumerated;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * Presents an abstract class that holds common information about the resource
 * 
 * @author Shchipunov Stanislav
 */
@Entity
@Inheritance(strategy = InheritanceType.JOINED)
@DiscriminatorColumn(name = "media_type", discriminatorType = DiscriminatorType.STRING)
@NamedQueries({
		@NamedQuery(name = "MediaInformation.findByTitle", query = "FROM MediaInformation WHERE LOWER(title) LIKE LOWER(:title)"),
		@NamedQuery(name = "MediaInformation.getCountByKeywords", query = "SELECT COUNT (mediaInformation) FROM MediaInformation mediaInformation INNER JOIN mediaInformation.keywords keyword WHERE keyword IN (:keywords)"),
		@NamedQuery(name = "MediaInformation.findByKeywords", query = "FROM MediaInformation mediaInformation INNER JOIN mediaInformation.keywords keyword WHERE keyword IN (:keywords)") })
@Getter
@Setter
@ToString(callSuper = true)
public abstract class MediaInformation extends AbstractEntity {

	private String title;

	/**
	 * Map that holds paths to the resource. As a key it uses format type (MP3
	 * as an example).
	 */
	@ElementCollection
	@CollectionTable(name = "path_to_resource")
	@MapKeyEnumerated(EnumType.STRING)
	@MapKeyColumn(name = "format_type")
	@Column(name = "value")
	private Map<FormatType, String> pathsToResource;

	/**
	 * Set of keywords associated to resource
	 */
	@ElementCollection
	@CollectionTable(name = "keyword", joinColumns = @JoinColumn(name = "media_information_id"))
	private Set<String> keywords = new HashSet<>();

	public String getPathToResource(FormatType formatType) {
		return pathsToResource.get(formatType);
	}
}
