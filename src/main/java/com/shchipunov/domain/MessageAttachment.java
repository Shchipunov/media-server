package com.shchipunov.domain;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import com.fasterxml.jackson.annotation.JsonBackReference;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * @author Shchipunov Stanislav
 */
@Entity
@Getter
@Setter
@ToString(callSuper = true)
public class MessageAttachment extends AbstractEntity {

	private String originalName;
	private String contentType;
	private long contentLength;
	private String pathToResource;
	@ManyToOne
	@JoinColumn(name = "message_id")
	@JsonBackReference
	private Message message;

	protected MessageAttachment() {

	}

	public MessageAttachment(String originalName, String contentType, long contentLength, String pathToResource) {
		this.originalName = originalName;
		this.contentType = contentType;
		this.contentLength = contentLength;
		this.pathToResource = pathToResource;
	}
}
