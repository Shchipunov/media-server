package com.shchipunov.domain;

import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import javax.persistence.Version;

import lombok.ToString;

/**
 * Presents a base class that implements common properties of objects of the
 * domain model
 * 
 * @author Shchipunov Stanislav
 */
@MappedSuperclass
@ToString
public abstract class AbstractEntity {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	@Version
	private Long version;

	public Long getId() {
		return id;
	}

	public Long getVersion() {
		return version;
	}

}
