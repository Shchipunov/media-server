package com.shchipunov.domain;

import java.sql.Date;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.OneToMany;

import com.fasterxml.jackson.annotation.JsonManagedReference;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * @author Shchipunov Stanislav
 */
@Entity
@Getter
@Setter
@ToString(callSuper = true)
public class Message extends AbstractEntity {

	private String sender;
	private String recipient;
	private String subject;
	private String text;
	
	/**
	 * List of attachments associated to the message
	 */
	@OneToMany(mappedBy = "message", cascade = CascadeType.ALL, orphanRemoval = true)
	@JsonManagedReference
	private List<MessageAttachment> attachments = new ArrayList<>();
	private Date creationDate;
	private boolean seen;

	public Message() {
		this.creationDate = Date.valueOf(LocalDate.now());
	}

	public boolean addAttachment(MessageAttachment attachment) {
		attachment.setMessage(this);
		return getAttachments().add(attachment);
	}

	public boolean removeAttachment(MessageAttachment attachment) {
		return getAttachments().remove(attachment);
	}
}
