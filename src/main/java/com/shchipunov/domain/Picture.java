package com.shchipunov.domain;

import javax.persistence.Basic;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Lob;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * Presents a class that holds scaled and full size instances of an image
 * 
 * @author Shchipunov Stanislav
 */
@Entity
@Getter
@Setter
@ToString(callSuper = true)
public class Picture extends AbstractEntity {

	@Lob
	@Basic(fetch = FetchType.LAZY)
	private byte[] scaledInstance;
	@Lob
	@Basic(fetch = FetchType.LAZY)
	private byte[] fullSizeInstance;

	protected Picture() {

	}

	public Picture(byte[] scaledInstance, byte[] fullSizeInstance) {
		this.scaledInstance = scaledInstance;
		this.fullSizeInstance = fullSizeInstance;
	}

}
