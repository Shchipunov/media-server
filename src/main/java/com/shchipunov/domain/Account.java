package com.shchipunov.domain;

import java.util.Arrays;
import java.util.Collection;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Id;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import lombok.ToString;

/**
 * Presents a class that holds account details
 * 
 * @author Shchipunov Stanislav
 */
@Entity
@ToString(exclude = "password")
public class Account implements UserDetails {

	private static final long serialVersionUID = 1L;

	@Id
	private String username;
	private String password;
	@Enumerated(EnumType.STRING)
	private Role role;

	@Override
	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	@Override
	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public Role getRole() {
		return role;
	}

	public void setRole(Role role) {
		this.role = role;
	}

	@Override
	public Collection<? extends GrantedAuthority> getAuthorities() {
		return Arrays.asList(new SimpleGrantedAuthority(getRole().toString()));
	}

	@Override
	public boolean isAccountNonExpired() {
		return true;
	}

	@Override
	public boolean isAccountNonLocked() {
		return true;
	}

	@Override
	public boolean isCredentialsNonExpired() {
		return true;
	}

	@Override
	public boolean isEnabled() {
		return true;
	}

	@Override
	public boolean equals(Object object) {
		if (object != null && object instanceof Account) {
			Account that = (Account) object;
			return this.username.equals(that.username);
		} else {
			return false;
		}
	}

	@Override
	public int hashCode() {
		return username.hashCode();
	}
}
