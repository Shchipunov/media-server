package com.shchipunov.domain;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CollectionTable;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * Presents a class that holds additional information about the resource
 * 
 * @author Shchipunov Stanislav
 */
@Entity
@Getter
@Setter
@ToString(callSuper = true)
public class AudioInformation extends MediaInformation {

	private String artist;
	private String genre;

	/**
	 * List of pictures associated to resource
	 */
	@ElementCollection(targetClass = Picture.class)
	@CollectionTable(name = "audio_picture", joinColumns = @JoinColumn(name = "audio_information_id"))
	private List<Picture> pictures = new ArrayList<>();

	public boolean addPicture(Picture picture) {
		return pictures.add(picture);
	}
}
